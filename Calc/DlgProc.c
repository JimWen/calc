#include <Windows.h>
#include <WindowsX.h>
#include "resource.h"
#include "DlgProc.h"
#include "Error.h"
#include "CalcVariable.h"
#include "Input.h"

/************************************************************************/
// 功能:对话框过程处理函数,将不同的消息分发给不同的函数处理
/************************************************************************/
INT_PTR CALLBACK DlgProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
		HANDLE_MSG(hwndDlg, WM_INITDIALOG, OnInitDialog);	//WM_INITDIALOG消息
		HANDLE_MSG(hwndDlg, WM_COMMAND, OnCommand);			//WM_COMMAND消息
	}

	return FALSE;
}

/************************************************************************/
// 功能:WM_INITDIALOG消息处理函数
/************************************************************************/
BOOL OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam) 
{
	/*设置程序图标*/
	SetClassLong(hwnd, GCL_HICON, 
				(LONG)LoadIcon((HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
							   MAKEINTRESOURCE(IDI_APPICON)));
	return TRUE;
}

/************************************************************************/
// 功能:WM_COMMAND消息处理函数
/************************************************************************/
void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch(id)
	{
	case IDB_CE:
		if(IsError())
		{
			//若发生了错误则清除错误
			SetErrorState(FALSE);
		}
		else
		{
			ClearEdit(hwnd, IDE_CURINPUT);
		}
		break;

	case IDB_C:
		if(IsError())
		{
			//若发生了错误则清除错误
			SetErrorState(FALSE);
		}
		else
		{
			Clear(hwnd, IDE_CURINPUT, IDE_MFLAG);
		}
		break;

	case IDCANCEL:
		EndDialog(hwnd, TRUE);
		break;
	}

	//若发生了错误，则报警并不予计算直到用<CE>或<C>清除错误
	if(IsError())
	{
		ShowError();
		return; 
	}

	switch (id)
	{
	case IDB_0:
	case IDB_1:
	case IDB_2:
	case IDB_3:
	case IDB_4:
	case IDB_5:
	case IDB_6:
	case IDB_7:
	case IDB_8:
	case IDB_9:
		InputNum(hwnd, IDE_CURINPUT, id-IDB_0);
		break;

	case IDB_POINT:
		InputPoint(hwnd, IDE_CURINPUT);
		break;

	case IDB_BACK:
		Backspace(hwnd, IDE_CURINPUT);
		break;

	case IDB_POS2NEG:
		Inverse(hwnd, IDE_CURINPUT);
		break;
	case IDB_SQRT:
		SqrtCur(hwnd, IDE_CURINPUT);
		break;
	case IDB_PERCENT:
		Percent(hwnd, IDE_CURINPUT);
		break;
	case IDB_RECI:
		Reci(hwnd, IDE_CURINPUT);
		break;

	case IDB_MC:
		MClear(hwnd, IDE_MFLAG);
		break;
	case IDB_MR:
		MRead(hwnd, IDE_CURINPUT);
		break;
	case IDB_MS:
		MSave(hwnd, IDE_MFLAG);
		break;
	case IDB_MPLUS:
		MPlus(hwnd, IDE_MFLAG);
		break;
	case IDB_MSUB:
		MSub(hwnd, IDE_MFLAG);
		break;

	case IDB_PLUS:
	case IDB_SUB:
	case IDB_MULTIPLY:
	case IDB_DIVIDE:
		InputOperator(hwnd, IDE_CURINPUT, id-IDB_PLUS);
		break;

	case IDB_EQUAL:
		Equal(hwnd, IDE_CURINPUT);
		break;

	//访问网站
	case IDM_WEBSITE:
		ShellExecute(hwnd, TEXT("Open"), TEXT("http://jimwen.net/tag.php?tag=Calc"), NULL, NULL, SW_NORMAL);
		break;
	}
}