#ifndef INPUT_H_H_H
#define INPUT_H_H_H

void InputNum(HWND hDlg, int id, int num);
void InputPoint(HWND hDlg, int id);
void ShowWarning();
void ShowCurInput(HWND hDlg, int id);
void ShowErrorMsg(HWND hDlg, int id);

void Backspace(HWND hDlg, int id);
void ClearEdit(HWND hDlg, int id);
void Clear(HWND hDlg, int id_cur, int id_m);

void Inverse(HWND hDlg, int id);
void SqrtCur(HWND hDlg, int id);
void Percent(HWND hDlg, int id);
void Reci(HWND hDlg, int id);

void DoubleToStr(char str[], double num);

void ShowMemory(HWND hDlg, int id);
void MClear(HWND hDlg, int id);
void MRead(HWND hDlg, int id);
void MSave(HWND hDlg, int id);
void MPlus(HWND hDlg, int id);
void MSub(HWND hDlg, int id);

void InputOperator(HWND hDlg, int id, int num);
BOOL Calc();
void ShowCalcOutput(HWND hDlg, int id);
void Equal(HWND hDlg, int id);
#endif