#include<Windows.h>
#include "CalcVariable.h"
#include "Input.h"
#include <stdio.h>
#include <Strsafe.h>
#include <math.h>
#include "Error.h"

/////////////////////////////////////////////////////////////////////////
//当前输入
/////////////////////////////////////////////////////////////////////////

/************************************************************************/
// 功能：输入数字时动态更新当前输入对中数字
/************************************************************************/
void InputNum(HWND hDlg, int id, int num)
{
	//取消计算状态，输入归0
	if (bCalcState == TRUE)
	{
		bCalcState = FALSE;
		memset(szShowText, 0, 2048);
		bPoint = FALSE;
	}

	//取消函数状态，输入归0
	if (bFuncState == TRUE)
	{
		bFuncState = FALSE;
		memset(szShowText, 0, 2048);
		bPoint = FALSE;
	}

	//取消内存读取状态，输入归0
	if (bMReadState == TRUE)
	{
		bMReadState = FALSE;
		memset(szShowText, 0, 2048);
		bPoint = FALSE;
	}

	//历史输入数字后加上刚刚输入的
	sprintf(szShowText, "%s%d", szShowText, num);

	//动态更新显示
	ShowCurInput(hDlg, id);
}

/************************************************************************/
// 功能：输入小数点
/************************************************************************/
void InputPoint(HWND hDlg, int id)
{ 
	//取消计算状态，输入归0
	if (bCalcState == TRUE)
	{
		bCalcState = FALSE;
		memset(szShowText, 0, 2048);
		bPoint = FALSE;
	}

	//取消函数状态，输入归0
	if (bFuncState == TRUE)
	{
		bFuncState = FALSE;
		memset(szShowText, 0, 2048);
		bPoint = FALSE;
	}

	//取消内存读取状态，输入归0
	if (bMReadState == TRUE)
	{
		bMReadState = FALSE;
		memset(szShowText, 0, 2048);
		bPoint = FALSE;
	}

	if (bPoint == FALSE)//无小数点时
	{
		sprintf(szShowText, "%s.", szShowText);
		bPoint = TRUE;
	}
	else
	{
		//有小数点时警告用户
		ShowWarning();
	}

	//动态更新显示
	ShowCurInput(hDlg, id);
}

/************************************************************************/
// 功能：响铃以警告用户当前输入有误
/************************************************************************/
void ShowWarning()
{
	MessageBeep(MB_ICONASTERISK);
}

/************************************************************************/
// 功能：当输入时动态显示当前输入
/************************************************************************/
void ShowCurInput(HWND hDlg, int id)
{
	SetDlgItemTextA(hDlg, id, szShowText);
}

/************************************************************************/
// 功能：输出当前的错误信息
/************************************************************************/
void ShowErrorMsg(HWND hDlg, int id)
{
	SetDlgItemTextA(hDlg, id, szErrorMessage);
}

/************************************************************************/
// 功能：当前输入后退一位
/************************************************************************/
void Backspace(HWND hDlg, int id)
{
	if (bCalcState==FALSE)
	{
		if ( 0!=strlen(szShowText))
		{
			//处理清楚小数点的情况
			if (bPoint==TRUE && szShowText[strlen(szShowText)-1]=='.')
			{
				bPoint = FALSE;
			}

			//回退一位
			strncpy(szShowText, szShowText, max((strlen(szShowText)-1), 0));
			szShowText[strlen(szShowText)-1]='\0';
		}

		if (0 != strlen(szShowText))
		{
			ShowCurInput(hDlg, id);//刷新显示
		}
		else
		{
			SetDlgItemTextA(hDlg, id, "0");//输入为0字符串时默认输入为0
		}
	}
}

/************************************************************************/
// 功能：清除当前输入窗口的输入
/************************************************************************/
void ClearEdit(HWND hDlg, int id)
{
	//当前输入置空
	memset(szShowText, 0, 2048);
	bPoint = FALSE;

	//动态更新显示
	ShowCurInput(hDlg, id);
}

/************************************************************************/
// 功能：计算器复位即清除所有
/************************************************************************/
void Clear(HWND hDlg, int id_cur, int id_m)
{
	InitCalcVariable();

	//动态更新显示
	ShowCurInput(hDlg, id_cur);
	ShowMemory(hDlg, id_m);
}

/************************************************************************/
// 功能：当前输入取反
/************************************************************************/
void Inverse(HWND hDlg, int id)
{
	//对当前输入计算并将计算结果重新置为当前输入
	dTempNum = atof(szShowText);
	dTempNum = -dTempNum;
	DoubleToStr(szShowText, dTempNum);

	ShowCurInput(hDlg, id);//刷新当前显示

	bFuncState = TRUE;//计算状态置为真
}

/************************************************************************/
// 功能：对当前输入取开平方
/************************************************************************/
void SqrtCur(HWND hDlg, int id)
{
	dTempNum = atof(szShowText);

	if (dTempNum < 0)
	{
		SetErrorState(TRUE);
		ShowError();
		strcpy(szErrorMessage, "错误,不能对小于0的数开平方");
		ShowErrorMsg(hDlg, id);
	}
	else
	{
		//对当前输入计算并将计算结果重新置为当前输入
		dTempNum = sqrt(dTempNum);
		DoubleToStr(szShowText, dTempNum);

		ShowCurInput(hDlg, id);//刷新当前显示

		bFuncState = TRUE;//计算状态置为真
	}
}

/************************************************************************/
// 功能：当前输入取为前一个输入的(当前输入)%
/************************************************************************/
void Percent(HWND hDlg, int id)
{
	//对当前输入计算并将计算结果重新置为当前输入
	dTempNum = atof(szShowText);
	dTempNum = dNum1 * dTempNum / 100;
	DoubleToStr(szShowText, dTempNum);

	ShowCurInput(hDlg, id);//刷新当前显示

	bFuncState = TRUE;//计算状态置为真
}

/************************************************************************/
// 功能：求当前输入的倒数
/************************************************************************/
void Reci(HWND hDlg, int id)
{
	//对当前输入计算并将计算结果重新置为当前输入
	dTempNum = atof(szShowText);
	dTempNum = 1/dTempNum;
	DoubleToStr(szShowText, dTempNum);

	ShowCurInput(hDlg, id);//刷新当前显示

	bFuncState = TRUE;//计算状态置为真
}

/************************************************************************/
// 功能：将Double转换成char字符串
/************************************************************************/
void DoubleToStr(char str[], double num)
{
	int i;

	sprintf(str, "%f", num);

	//去除多余的0 
	for (i= strlen(str)-1; i>=0; i--)
	{
		if (str[i] == '0')
		{
			str[i] = '\0';
		}
		else
		{
			break;
		}
	}
	if (str[i] == '.')
	{
		str[i] = '\0';
	}
}

/************************************************************************/
// 功能：显示当前内存缓冲区是否有内容
/************************************************************************/
void ShowMemory(HWND hDlg, int id)
{
	if(TRUE == bMState)
	{
		SetDlgItemTextA(hDlg, id, "M");
	}
	else
	{
		SetDlgItemTextA(hDlg, id, " ");
	}
}

/************************************************************************/
// 功能：清空当前内存缓冲区
/************************************************************************/
void MClear(HWND hDlg, int id)
{
	dMemNum = 0;
	bMState = FALSE;

	ShowMemory(hDlg, id);//更新内存标志区
}

/************************************************************************/
// 功能：读取当前内存缓冲区作为当前输入
/************************************************************************/
void MRead(HWND hDlg, int id)
{
	//取消计算状态，输入归0
	if (bCalcState == TRUE)
	{
		bCalcState = FALSE;
		memset(szShowText, 0, 2048);
		bPoint = FALSE;
	}

	//取消函数状态，输入归0
	if (bFuncState == TRUE)
	{
		bFuncState = FALSE;
		memset(szShowText, 0, 2048);
		bPoint = FALSE;
	}

	if (bMState == TRUE)
	{
		DoubleToStr(szShowText, dMemNum);
		ShowCurInput(hDlg, id);
	}

	bMReadState = TRUE;//置于内存读取状态
}

/************************************************************************/
// 功能：当前输入存储为当前内存缓冲区
/************************************************************************/
void MSave(HWND hDlg, int id)
{
	dMemNum = atof(szShowText);

	if ((double)0 != dMemNum)
	{
		bMState = TRUE;
	}
	else
	{
		bMState = FALSE;
	}

	ShowMemory(hDlg, id);//更新内存标志区
}

/************************************************************************/
// 功能：当前内存缓冲区加上当前输入作为缓冲区
/************************************************************************/
void MPlus(HWND hDlg, int id)
{
	dMemNum += atof(szShowText);

	if ((double)0 != dMemNum)
	{
		bMState = TRUE;
	}
	else
	{
		bMState = FALSE;
	}

	ShowMemory(hDlg, id);//更新内存标志区
}

/************************************************************************/
// 功能：当前内存缓冲区减去当前输入作为缓冲区
/************************************************************************/
void MSub(HWND hDlg, int id)
{
	dMemNum -= atof(szShowText);

	if ((double)0 != dMemNum)
	{
		bMState = TRUE;
	}
	else
	{
		bMState = FALSE;
	}

	ShowMemory(hDlg, id);//更新内存标志区
}

/////////////////////////////////////////////////////////////////////////
//计算输出
/////////////////////////////////////////////////////////////////////////

/************************************************************************/
// 功能：输入计算符时更新相关计算数和计算符
/************************************************************************/
void InputOperator(HWND hDlg, int id, int num)
{
	if (FALSE == bCalcState)
	{
		if (iOperate == -1)//之前没输入操作符
		{
			dNum1 = atof(szShowText);//获得计算数1
			iOperate = num;

			bCalcState = TRUE;//置当前处于刚刚完成一次计算的状态(每输入一次计算符，dNum1得到一次值，当做完成一次计算)

			//刷新显示
			ShowCalcOutput(hDlg, id);
		}
		else
		{
			if (TRUE == Calc())
			{
				iOperate = num;

				bCalcState = TRUE;//置当前处于刚刚完成一次计算的状态(每输入一次计算符，dNum1得到一次值，当做完成一次计算)

				//刷新显示
				ShowCalcOutput(hDlg, id);
			}
			else
			{
				ShowError();
				ShowErrorMsg(hDlg, id);
			}
		}
	} 
	else
	{
		iOperate = num;
	}
}

/************************************************************************/
// 功能：根据当前的两个计算数和符号计算结果
/************************************************************************/
BOOL Calc()
{
	if (bCalcState == FALSE)
	{
		dNum2 = atof(szShowText);//获得计算数2

		switch (iOperate)
		{
		case 0:
			dNum1 += dNum2;
			break;
		case 1:
			dNum1 -= dNum2;
			break;
		case 2:
			dNum1 *= dNum2;
			break;
		case 3:
			if (dNum2 == (double)0)//除数不能为0
			{
				strcpy(szErrorMessage, "错误,除数不能为0!") ;
				SetErrorState(TRUE);
				return FALSE; 
			}
			else
			{
				dNum1 /= dNum2;
			}
			break;
		}
	}

	return TRUE;
}

/************************************************************************/
// 功能：显示每次计算结果
/************************************************************************/
void ShowCalcOutput(HWND hDlg, int id)
{
	//if (bCalcState == TRUE)
	{
		DoubleToStr(szShowText, dNum1);

		SetDlgItemTextA(hDlg, id, szShowText);
	}
}

/************************************************************************/
// 功能：将当前计算数和计算符号计算并显示结果
/************************************************************************/
void Equal(HWND hDlg, int id)
{
	if (iOperate == -1)//之前没输入操作符
	{
		bCalcState = TRUE;//置当前处于刚刚完成一次计算的状态(每输入一次计算符，dNum1得到一次值，当做完成一次计算)

		//刷新显示
		ShowCalcOutput(hDlg, id);
	}
	else
	{
		if (TRUE == Calc())
		{
			iOperate = -1;

			bCalcState = TRUE;//置当前处于刚刚完成一次计算的状态(每输入一次计算符，dNum1得到一次值，当做完成一次计算)

			//刷新显示
			ShowCalcOutput(hDlg, id);
		}
		else
		{
			ShowError();
			ShowErrorMsg(hDlg, id);
		}
	}
}